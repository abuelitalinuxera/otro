from django.shortcuts import render

from django.views.generic import TemplateView

# Create your views here.

def inicio_views(request):
	return render(request, 'home/home.dj.html', {})
	
class HomeView(TemplateView):
	template_name = 'home/home.dj.html'

class NosotresView(TemplateView):
	template_name = 'home/nosotres.dj.html'

class ProyectoView(TemplateView):
	template_name = 'home/proyecto.dj.html'

class ContactoView(TemplateView):
	template_name = 'home/contacto.dj.html'
