from django.conf.urls import url
from django.urls import path
from apps.home import views

app_name='home'

urlpatterns = [
	#path('', views.inicio_views, name='index'),
	path('', views.HomeView.as_view(), name='index'),
	path('nosotres', views.NosotresView.as_view(), name='nosotres'),
	path('proyecto', views.ProyectoView.as_view(), name='proyecto'),
	path('contacto', views.ContactoView.as_view(), name='contacto'),

	]

	
