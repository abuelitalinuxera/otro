# script para generar subcarpetas app, templates, statics, media y urls

templates=1
statics=1
media=0
urls=1

while getopts a:t:s:m:u option
do
case "${option}"
in
	a) app=${OPTARG};; #nombre que tendrá la aplicación
	t) templates=${OPTARG};; #OPTARG debe estar en mayúscula para que funcione
	s) statics=${OPTARG};;
	m) media=${OPTARG};;
	u) urls=${OPTARG};;
esac
done

echo "Creating $app folder"
mkdir ./apps/$app
echo "realizando startapp a manage.py"
python manage.py startapp $app ./apps/$app #para crear la aplicación de otro directorio, se debe crear antes la carpeta con el nombre de la aplicación
#dentro del manage.py está la automatización de los archivos y carpetas que se escribirán dentro de ./apps/$app

if [ $templates -eq 1 ] 
then
	echo "creating templates folder"
	mkdir ./apps/$app/templates
	echo "creating $app folder inside templates folder"
	mkdir ./apps/$app/templates/$app
fi

if [ $statics -eq 1 ]
then 
	echo "creating statics folder"
	mkdir ./apps/$app/statics
	echo "creating $app folder inside statics folder"
	mkdir ./apps/$app/statics/$app
fi

if [ $media -eq 1 ] 
then
	echo "creating media folder"
	mkdir ./apps/$app/media
	echo "creating $app folder inside media folder"
	mkdir ./apps/$app/media/$app
fi

if [ $urls -eq 1 ]
then
	echo "creating urls file"
	touch ./apps/$app/urls.py
fi
